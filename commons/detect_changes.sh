#!/bin/bash
# @author bgohier
# @date 20/07/2018
# @desc Detect files modifications into folders.

function usage()
{
    echo "USAGE: $0 [-e excludes] [-M max_age (seconds)] [-m min_age (seconds)] [-h] target_paths
    
Find files modified from given time into paths.

PARAMETERS:
    target_path:        The target paths list in which to look for modifications

OPTIONS:
    -h:                 Display this help
    -e <paths,>:        Exclude paths from research
    -m <min_age>:       The minimum of age in seconds (default 3)
    -M <max_age>:       The maximum of age in seconds (default 15)
"
}

EXCLUDES_ARGS=""
MIN_AGE=3
MAX_AGE=15

while getopts ":M:m:e:h" opt; do
    case "${opt}" in
    h)
        usage
        exit 0
        ;;
    e)
        EXCLUDES_ARGS="${EXCLUDES_ARGS} ${OPTARG}"
        ;;
    M)
        if [ ${OPTARG} -eq ${OPTARG} ] 2>/dev/null ; then
            MAX_AGE=${OPTARG}
        else
            >&2 echo "WARNING: Option '-M' require numeric value"
        fi
        ;;
    m)
        if [ ${OPTARG} -eq ${OPTARG} ] 2>/dev/null ; then
            MIN_AGE=${OPTARG}
        else
            >&2 echo "WARNING: Option '-m' require numeric value"
        fi
        ;;
    *)
        >&2 echo "WARNING: Unknown option -${OPTARG}"
        ;;
    esac
done
shift $[OPTIND-1]

if [ $# -lt 1 ] ; then
    >&2 echo "ERROR: Please specify at least one target path"
    exit 1
fi

declare -a TARGET_PATHS=($*)
declare -a EXCLUDES=(${EXCLUDES_ARGS})
EXCLUDES_OPTS=""
if [ ${#EXCLUDES[@]} -gt 0 ] ; then
    EXCLUDES_OPTS="! -path $(echo ${EXCLUDES[@]} | sed 's/ / ! -path /g')"
fi

RESULTS=""

for target_path in ${TARGET_PATHS[@]} ; do
    result="$(find ${target_path} -type f ! -newermt "-${MIN_AGE} seconds" -newermt "-${MAX_AGE} seconds" ${EXCLUDES_OPTS} 2>&1)"
    exec=$?
    if [ ${exec} -ne 0 ] ; then
        >&2 echo "WARNING: Cannot search in target path '${target_path}':
    ${result}"
    else
        [ ! -z "${result}" ] && RESULTS="${RESULTS}
${result}"
    fi
done

echo "${RESULTS}" | tail -n +2

exit 0
