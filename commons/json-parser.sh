#!/bin/bash
# @author bgohier
# @date 26/08/2016
# @desc Parse a json string using python.
# Usage: $0 <JSON_CONTENT> <RESULT_PATTERN>
# Changes:
#     - 26/08/2016 [bgohier]: Allow '-' char in key
# With parameters:
#     JSON_CONTENT:   A valid JSON as string
#                     eg. "{"item1":"value1","item2":"value2"}"
#     RESULT_PATTERN: A pattern to look in the python json object as res:
#                     eg. "res.item2" will return "value2"
#                     Note: python command can be used:
#                     eg. "len(res)" will return "2"
##

nb=$#
json_txt="$1"
pattern="$2"

HELP_ARG="--help"
HELP_ARG_SHORT="-h"

is_help=false
arg="-"
while [ ! -z "${arg}" ] ; do
	arg="$1"
	case "${arg}" in
		${HELP_ARG}|${HELP_ARG_SHORT})
			is_help=true
			;;
		*)
			;;
	esac
	shift
done

if [ ${nb} -lt 2 ] || ${is_help} ; then
	echo "Usage: $0 <JSON_CONTENT> <RESULT_PATTERN>
With parameters:
	JSON_CONTENT:	A valid JSON as string
			eg. \"{\"item1\":\"value1\",\"item2\":\"value2\"}\"
	RESULT_PATTERN:	A pattern to look in the python json object as res:
			eg. \"res.item2\" will return \"value2\"
			Note: python command can be used:
			eg. \"len(res)\" will return \"2\""
	exit 2
fi

py_arg="$(echo "${pattern}" | sed -e 's/\.\(\(\w\|-\)\+\)/["\1"]/g')"
py_cmd="
import json,sys
res=json.load(sys.stdin)
try:
	print ${py_arg}
	exit(0)
except KeyError:
	print 'Key \"${pattern}\" can not be reached.'
	exit(1)
"

ret="$(echo "${json_txt}" | python -c "${py_cmd}")"
ret_code=$?
echo "${ret}"
exit ${ret_code}
